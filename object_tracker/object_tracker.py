# -*- coding: utf-8 -*-

__version__ = '0.1.0'


class ObjectTracker(object):
    """Singleton class to track current live objects per cluster."""

    class __ObjectTracker:

        def __init__(self):
            self.objects = {}

        def add_object(self, identifier, obj):
            """Add object that goes into class dict."""
            self.objects[identifier] = obj

        def get_object(self, identifier):
            """Get object using identifier as identifier."""
            return self.objects.get(identifier)

        def pop_object(self, identifier):
            """Pop object if it exists."""
            return self.objects.pop(identifier, None)

    instance = {}

    def __new__(cls, cluster):
        """Instantiate only once per cluster."""
        if not ObjectTracker.instance.get(cluster):
            ObjectTracker.instance[cluster] = ObjectTracker.__ObjectTracker()
        return ObjectTracker.instance[cluster]
