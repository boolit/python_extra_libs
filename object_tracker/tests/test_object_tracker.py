# -*- coding: utf-8 -*-

import unittest

import object_tracker


class TestObjectTracker(unittest.TestCase):
    """Test cases for ObjectTracker class."""

    def setUp(self):
        """Set up data for ObjectTracker class tests."""
        self.ObjectTracker = object_tracker.ObjectTracker
        super(TestObjectTracker, self).setUp()

    def test_add_object(self):
        """Test if objects dict is updated after using add_object."""
        # Because it is singleton, we will always reference same
        # instance
        self.ObjectTracker('c1').add_object(1, 'abc')
        self.assertEqual(self.ObjectTracker('c1').objects, {1: 'abc'})
        self.ObjectTracker('c1').add_object(2, 'A54')
        self.assertEqual(
            self.ObjectTracker('c1').objects, {1: 'abc', 2: 'A54'})
        # Add object for cluster 'c2'
        self.ObjectTracker('c2').add_object(1, 'abcd')
        self.assertEqual(self.ObjectTracker('c2').objects, {1: 'abcd'})
        self.ObjectTracker('c2').add_object(11, 'A545')
        self.assertEqual(
            self.ObjectTracker('c2').objects, {1: 'abcd', 11: 'A545'})

    def test_get_object(self):
        """Test if we can retrieve object using identifier."""
        self.assertEqual(self.ObjectTracker('c1').get_object(1), 'abc')
        self.assertEqual(self.ObjectTracker('c1').get_object(2), 'A54')
        self.assertEqual(self.ObjectTracker('c2').get_object(1), 'abcd')
        self.assertEqual(self.ObjectTracker('c2').get_object(11), 'A545')

    def test_pop_object(self):
        """Test if object popping works properly."""
        self.assertEqual(self.ObjectTracker('c1').pop_object(1), 'abc')
        self.assertEqual(self.ObjectTracker('c1').get_object(1), None)
        self.assertEqual(self.ObjectTracker('c1').objects, {2: 'A54'})
        self.assertEqual(self.ObjectTracker('c2').pop_object(1), 'abcd')
        self.assertEqual(self.ObjectTracker('c2').get_object(1), None)
        self.assertEqual(self.ObjectTracker('c2').objects, {11: 'A545'})

    def test_tracker_is_singleton(self):
        """Test if ObjectTracker is always same instance per cluster."""
        self.assertEqual(self.ObjectTracker('c1'), self.ObjectTracker('c1'))
        self.assertNotEqual(self.ObjectTracker('c1'), self.ObjectTracker('c2'))

    def test_tracker_required_cluster(self):
        """Test that we need arg for ObjectTracker."""
        self.assertRaises(TypeError, self.ObjectTracker)

if __name__ == '__main__':
    unittest.main()
