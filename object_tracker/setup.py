# -*- coding: utf-8 -*-
from distutils.core import setup

setup(
    name='Python Extra Libraries',
    version='0.1',
    py_modules=['object_tracker', ],
    author='Andrius Laukavičius',
    author_email='dev@boolit.eu',
    url='git@bitbucket.org:boolit/python_extra_libs.git',
    license='MIT',
    long_description=open('README.txt').read(),
)
