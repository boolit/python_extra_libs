# -*- coding: utf-8 -*-
from distutils.core import setup

setup(
    name='Numeric String Parser',
    version='0.1',
    py_modules=['numeric_string_parser'],
    author='Paul McGuire',
    url='git@bitbucket.org:boolit/python_extra_libs.git',
    license='MIT',
    long_description=open('README.txt').read(),
)
