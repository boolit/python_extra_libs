# -*- coding: utf-8 -*-
from distutils.core import setup

setup(
    name='IBAN',
    version='0.1',
    py_modules=['iban'],
    author='Thomas Günther',
    author_email='tom@toms-cafe.de',
    url='git@bitbucket.org:boolit/python_extra_libs.git',
    license='GNU',
    long_description=open('README.txt').read(),
)
